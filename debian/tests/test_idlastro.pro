PRO test_remove
  print, 'REMOVE'
  index = [2,4,6,4]
  v = [1,3,4,3,2,5,7,3]
  REMOVE, index,v
  print, v
  if not min(v eq [1,3,3,5,3]) then exit, status=1
END

PRO test_baryvel
  print, 'JDCNF'
  jdcnv, 1994, 2, 15, 0, jd
  if jd ne 2449398.5 then exit, status=1
  
  print, 'BARYVEL'
  baryvel, jd, 2000, vh, vb
  print, vh
  if max (abs(vh - [-17.072426, -22.811209, -9.8893154])) gt 1e-6 then $
     exit, status=1
  print, vb
  if max (abs(vb - [-17.080834, -22.804708, -9.8862582])) gt 1e-6 then $
     exit, status=1
END

PRO test_brecess
  print, 'BPRECESS'
  mu_radec = 100D* [ -15D*.0257, -0.090 ]
  ra = ten(13, 42, 12.740)*15.D 
  dec = ten(8, 23, 17.69)
  bprecess, ra, dec, ra1950, dec1950, mu_radec = mu_radec
  print, ra1950, dec1950
  if abs(ra1950 - 204.93553) gt 1e-4 then exit, status=1
  if abs(dec1950 - 8.6412872) gt 1e-6 then exit, status=1
END

PRO test_calz_unred
  print, 'CALZ_UNRED'
  w = 1200 + findgen(40)*50 
  f = w*0 + 1
  calz_unred, w, f, -0.1, fnew
  print, fnew
  if abs(fnew[0] - 0.327510) gt 1e-6 then exit, status=1
  if abs(fnew[6] - 0.386050) gt 1e-6 then exit, status=1
  if abs(fnew[12] - 0.421724) gt 1e-6 then exit, status=1
END

PRO test_zang
  print, 'ZANG'
  z = zang(50,1.5, Lambda = 0,omega_m = 0.3)
  print, z, abs(z-6.58)
  if abs(z-6.58) gt 1e-3 then exit, status=1  
END

PRO test_fits1
  orig = randomn(0,64,128)
  FITS_WRITE,'f1.fits',orig
  FITS_OPEN, 'f1.fits', fcb
  FITS_READ, fcb, data, header
  file_delete, 'f1.fits'
  print, header
  if sxpar(header, 'NAXIS') ne 2 then exit, status = 1
  if sxpar(header, 'NAXIS1') ne 64 then exit, status = 1
  if sxpar(header, 'NAXIS2') ne 128 then exit, status = 1
  if max(ABS(orig - data)) gt 1e-6 then exit, status = 1
END
  
PRO test_fits2
  orig = randomn(0,64,128)
  writefits, 'f2.fits', orig
  data = READFITS( 'f2.fits', header)
  file_delete, 'f2.fits'
  print, header
  if sxpar(header, 'NAXIS') ne 2 then exit, status = 1
  if sxpar(header, 'NAXIS1') ne 64 then exit, status = 1
  if sxpar(header, 'NAXIS2') ne 128 then exit, status = 1
  if max(ABS(orig - data)) gt 1e-6 then exit, status = 1
END

PRO test_isarray
  print, 'ISARRAY'
  if isarray(1) then exit, status = 1
  if not isarray([1,2,3]) then exit, status = 1
  if isarray(a) then exit, status = 1
END

PRO test_ymd2dn
  print, "YMD2DN"
  dy = ymd2dn(1988, 11, 5)
  if dy ne 310 then exit, status = 1
END

PRO test_repchr
  print, "REPCHR"
  if repchr("erogoree", "b") ne "erogoree" then exit, status = 1
  if repchr("ebrogoree", "b") ne "e rogoree" then exit, status = 1
  if repchr("ebbrogoree", "b") ne "e  rogoree" then exit, status = 1
  if repchr("ebbrobgobree", "b") ne "e  ro go ree" then exit, status = 1
  if repchr("ebbrobgobree", "b", "c") ne "eccrocgocree" then exit, status = 1
END

PRO test_adstring
  print, 'ADSTRING'
  print, adstring(+0.23)
  if adstring(+0.23) ne '+00 13 48.0' then $
     exit, status=1
  print, adstring(30.42,+0.23)
  if adstring(30.42,+0.23) ne ' 02 01 40.8   +00 13 48.0' then $
     exit, status=1
  print, adstring(30.42,-1.23,1)
  if adstring(30.42,-1.23,1) ne ' 02 01 40.80  -01 13 48.0' then $
     exit, status=1
END

PRO test_f_format
  print, "F_FORMAT"
  fmt = F_FORMAT( 5.2e-3, 4.2e-2, factor )
  if fmt ne 'F5.2' then exit, status = 1
  if abs(factor - 0.01) gt 1e-6 then exit, status = 1
END

PRO test_idlastro
  test_remove
  test_baryvel
  test_brecess
  test_calz_unred
  test_zang
  test_fits1
  test_fits2
  test_isarray
  test_ymd2dn
  test_repchr
  ;  test_adstring ; fails with gdl-0.9.5
  test_f_format
  exit, status=0
END
